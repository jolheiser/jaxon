# Day 8

The day has gone very well, I feel like I am making solid progress.

Jaxon was a staple to our household, and I think he will be missed for a long time to come. 
However, he would want us to be as happy, and I think we are starting to get back to a sense of normal.

Rest in peace, Jaxon. We love you very much.

![adoption](../images/adoption.jpg)

![superhero](../images/superhero.jpg)

![brothers](../images/brothers.jpg)