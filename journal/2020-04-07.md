# Day 0 - Night

The first night without Jaxon was not as hard as I thought, though I think it was mostly due to lingering shock and because I was very tired.
I slept facing his side of the bed, with my arm out as it always was. He was not there, but it was a slight comfort.
He would always get *so* jealous when I would face Nicole that I was very rarely able to do so.
Now I am able to, but I don't think I will be comfortable sleeping facing away from Jaxon's side for a few nights.

Thankfully Brad slept down at my feet, so I was able to feel some cat-warmth, silly though it may sound.

# Day 1

Today was much harder than yesterday. There were many tears, talks of getting another cat, more tears because what I really wanted was Jaxon.

## The bad

Today we took Jaxon to the vet for cremation. We should get his ashes back in three or so weeks.
It was very hard, up to this point I was able to sneak to his box and still pet his fur.
I had to pet him and breathe his dusty fur for the last time. 

Putting down Bennet for his nap, I went through my phone to find all the pictures of Jaxon and try to capture the memory they contained.
I cried a lot while going through, but was surprised and happy to see how many photos I had of him.

Brad hid under the bed for most of the day. I'm not sure whether it's because he's Brad and aloof, or if he's a little freaked out.
I'm guessing it's the latter, but he did come out briefly to eat, and he ate treats whenever presented.

It's very hard to put Bennet down for naps or bedtime, because I remember always being anxious that Jaxon would
get fed up waiting for me and meow or scratch the door and wake Bennet up.
Now I would do anything to get interrupted again.

## The good

We were able to go on **two** walks today! Bennet loved taking his truck and trailer along.

![truck](../images/shoelace-truck-bennet.jpg)

When I'm with my wife and son, it's bearable. I can think of him without crying *every* time, although I still tear up more often than not.

Most of the photos I mentioned above were of Jaxon sleeping or cuddled up, so I am very happy to see he had a very nice life, full of happiness.

I decided to start this journal, to help me walk through this time but also to remember Jaxon as he was. To write down my thoughts
so that I never forget or let his memories wash away.

Brad came out of hiding around 8:00 PM and is currently sitting with his tail touching my foot, so I *think* that's affection?

Jaxon was my very best friend, and I can only hope I gave him a fraction of the happiness that he gave me.

![friend](../images/friend.jpg)

[Day 2](2020-04-08.md)