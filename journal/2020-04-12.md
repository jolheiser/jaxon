# Day 6

Today is my birthday. I've never really been a big birthday person, so not doing much is pretty normal.

I missed Jaxon today, a lot. It would have been nice to take my nap next to him, or watch a show with him on my lap.

Instead, I'll put some photos of Jaxon to commemorate.

Watching from the headboard

![birthday 1](../images/birthday-1.jpg)

Such good naps his tongue would stick out

![birthday 2](../images/birthday-2.jpg)

Sleeping on his cat tree that I made for him

![birthday 3](../images/birthday-3.jpg)

Stretched out on a nice blanket, cozy

![birthday 4](../images/birthday-4.jpg)

-----

Tomorrow will be one week since he passed away. In some ways, it feels like it was forever ago. In some ways, it still
seems like it was yesterday. 

I think I'm doing better than I expected to be at this point, but I think I really have my wife and son to thank for that.
They've been very uplifting, and I am very thankful to have them.

Brad has also started to come out of his shell a little. I think he spent so long as a "second" to Jaxon that he never really
found his own way, but now he's really starting to break out little by little. He's currently sitting right behind me, 
something I don't think he's *ever* done before.

Things will be okay.

![okay](../images/okay.jpg)

[Day 7](2020-04-13.md)