# Jaxon - A Grief Journal

Five years and eight months old.  
A part of my life from 11-22-2014 to 04-06-2020.

This is my git/markdown grief journal for my cat, my very best friend, Jaxon. It will probably be a lot of rambling, and
I have a feeling many English majors would be deeply offended.

But that's okay, because most of this will probably be typed through tears anyways.

> I am a kitty cat, this is my kitty fat, it goes *pat pat pat*

Jaxon was a ~~world~~ house-renowned drummer.

## Nicknames

* Jack
* Jax
* Jaxon-maxon (pudding and paxon)
* Stinker-dinker (this nickname was also given to my son)
* Stinko-blinko
* Jacko-macko
* Jackle-mackle(-more)
* Jackly-mackly-moe
* Action Jaxon
* Stinker-cat Jaxon
* Pitter-patter squitter-squatter (kitter-catter)
* Scooter-booter

## The Beginning

The very first photo I have of Jaxon after he came home in September of 2014.

![first](images/first.jpg)

Maybe a little better of a photo, fairly soon after coming home.

![baby boy](images/baby.jpg)

## The End

The very last photo I have of Jaxon before he passed away in April of 2020.
He had become accustomed to sleeping right next to me, with his head on the pillow.

![last](images/last.jpg)

Maybe a better idea of his usual napping method.

![sleepers](images/sleepers.jpg)

### The Journal

[Death Details](journal/0000-00-00.md)

[Day 0](journal/2020-04-06.md)